package io.gitlab.bluemonster122.sparked.common.util;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.items.ItemHandlerHelper;

public class StackUtils {

    public static void dropItem(World world, ItemStack stack, double x, double y, double z, boolean instantPickup) {
        EntityItem drop = new EntityItem(world);
        drop.setItem(stack);
        drop.setPosition(x, y, z);
        if (instantPickup) {
            drop.setPickupDelay(0);
        }
        world.spawnEntity(drop);
    }

    public static void giveItemToPlayer(EntityPlayer player, ItemStack item) {
        ItemHandlerHelper.giveItemToPlayer(player, item);
    }
}
