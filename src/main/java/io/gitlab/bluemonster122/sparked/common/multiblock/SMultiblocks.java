package io.gitlab.bluemonster122.sparked.common.multiblock;

import com.google.common.collect.ImmutableList;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.Vec3i;

public class SMultiblocks {

    public static final Multiblock GOBLIN_MULTIBLOCK = new Multiblock("goblin_ritual");

    public static void populate() {
        GOBLIN_MULTIBLOCK.setForm(ImmutableList.of(Vec3i.NULL_VECTOR, new Vec3i(-1, -1, -1), new Vec3i(-1, -1, 0), new Vec3i(-1, -1, 1), new Vec3i(0, -1, -1), new Vec3i(0, -1, 0), new Vec3i(0, -1, 1), new Vec3i(1, -1, -1), new Vec3i(1, -1, 0), new Vec3i(1, -1, 1)), ImmutableList.of(bs -> bs.getBlock().equals(Blocks.FIRE), bs -> bs.getBlock().equals(Blocks.STONEBRICK), bs -> bs.getBlock().equals(Blocks.STONEBRICK), bs -> bs.getBlock().equals(Blocks.STONEBRICK), bs -> bs.getBlock().equals(Blocks.STONEBRICK), bs -> bs.getBlock().equals(Blocks.BRICK_BLOCK), bs -> bs.getBlock().equals(Blocks.STONEBRICK), bs -> bs.getBlock().equals(Blocks.STONEBRICK), bs -> bs.getBlock().equals(Blocks.STONEBRICK), bs -> bs.getBlock().equals(Blocks.STONEBRICK)));
    }
}
