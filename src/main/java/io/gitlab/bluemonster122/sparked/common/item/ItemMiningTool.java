package io.gitlab.bluemonster122.sparked.common.item;

import com.google.common.collect.ImmutableSet;
import net.minecraft.block.Block;
import net.minecraft.block.BlockObsidian;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;


/**
 * This is a mining tool that is effective as a shovel and a pickaxe. It will be repairable with a crystal and wood, and it will use the spellbook to do this.
 *
 * @author BlueMonster122
 * @version 1.0 Created on 01/07/2018
 */
public class ItemMiningTool extends SItem {

    @GameRegistry.ObjectHolder("minecraft:torch")
    public static final Item TORCH = null;
    @GameRegistry.ObjectHolder("minecraft:blaze_powder")
    public static final Item BLAZE_POWDER = null;
    @GameRegistry.ObjectHolder("sparked:stick_magical_wood")
    public static final Item MAGIC_STICK = null;

    private static final String NBT_DAMAGE = "damage";
    private static final String NBT_MODE = "mode";
    private static final String MODE_1x1 = "1x1";
    private static final String MODE_1x2 = "1x2";
    private static final String MODE_3x3 = "3x3";

    private static final int MAX_DURABILITY = 1024;

    public ItemMiningTool() {
        super();
        this.setMaxStackSize(1);
        this.setHarvestLevel("pickaxe", 30);
        this.setHarvestLevel("shovel", 30);
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        BlockPos placementPos = pos.offset(facing);
        if (worldIn.getBlockState(pos).getBlock().isReplaceable(worldIn, pos)) {
            placementPos = pos;
        }
        if (placementPos.equals(pos) || worldIn.isAirBlock(placementPos)) {
            IItemHandler itemHandler = player.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
            ItemStack torch = ItemStack.EMPTY;
            for (int i = 0; i < itemHandler.getSlots(); i++) {
                ItemStack stackInSlot = itemHandler.extractItem(i, 1, true);
                if (stackInSlot.getItem().equals(TORCH)) {
                    torch = itemHandler.extractItem(i, 1, player.isCreative());
                    break;
                }
            }
            if (torch != ItemStack.EMPTY) {
                IBlockState placedState = Blocks.TORCH.getStateForPlacement(worldIn, placementPos, facing, hitX, hitY, hitZ, torch.getMetadata(), player, hand);
                worldIn.setBlockState(placementPos, placedState, 3);
                SoundType soundtype = Blocks.TORCH.getSoundType(placedState, worldIn, pos, player);
                worldIn.playSound(player, pos, soundtype.getPlaceSound(), SoundCategory.BLOCKS, (soundtype.getVolume() + 1.0F) / 2.0F, soundtype.getPitch() * 0.8F);
                return EnumActionResult.SUCCESS;
            }
        }
        return EnumActionResult.PASS;
    }

    @Override
    public float getDestroySpeed(ItemStack stack, IBlockState state) {
        if (getDamage(stack) >= getMaxDamage(stack)) {
            return 0F;
        }
        if (stack.getItem().equals(this)) {
            Block block = state.getBlock();
            if (block instanceof BlockObsidian || block.isToolEffective("pickaxe", state) || block.isToolEffective("shovel", state)) {
                return 25f;
            }
        }
        return 1F;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        ItemStack stack = playerIn.getHeldItem(handIn);
        if (playerIn.isSneaking()) {
            cycleMode(stack);
        } else {
            if (!worldIn.isRemote && getDamage(stack) > getMaxDamage(stack)) {
                IItemHandler itemHandler = playerIn.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
                int blazepowder = -1; // TODO: make this crystals
                int wood = -1; // TODO: make this magical wood;
                for (int i = 0, slots = itemHandler.getSlots(); i < slots && (wood == -1 || blazepowder == -1); i++) {
                    ItemStack itemStack = itemHandler.extractItem(i, 2, true);
                    if (blazepowder == -1 && itemStack.getItem().equals(BLAZE_POWDER) && itemStack.getCount() >= 1) {
                        blazepowder = i;
                    } else if (wood == -1 && itemStack.getItem().equals(MAGIC_STICK) && itemStack.getCount() >= 2) {
                        wood = i;
                    }
                }
                if (wood != -1 && blazepowder != -1) {
                    if (!playerIn.isCreative()) {
                        itemHandler.extractItem(wood, 1, false);
                        itemHandler.extractItem(blazepowder, 1, false);
                    }
                    setDamage(stack, getDamage(stack) - 1024);
                }
            }
        }
        return ActionResult.newResult(EnumActionResult.SUCCESS, stack);
    }

    @Override
    public boolean isDamageable() {
        return true;
    }

    @Override
    public boolean onBlockDestroyed(ItemStack stack, World worldIn, IBlockState state, BlockPos pos, EntityLivingBase entityLiving) {
        if (!worldIn.isRemote && state.getBlockHardness(worldIn, pos) > 1F) {
            NBTBase tagInfo = getTagInfo(stack, NBT_MODE);
            if (tagInfo != null && tagInfo instanceof NBTTagString) {
                switch (((NBTTagString) tagInfo).getString()) {
                    case MODE_1x1:
                        this.setDamage(stack, getDamage(stack) + 1);
                        break;
                    case MODE_1x2:
                        EnumFacing facing = EnumFacing.getDirectionFromEntityLiving(pos, entityLiving);
                        if (facing.getAxis().isHorizontal()) {
                            if (worldIn.destroyBlock(pos.down(), true)) {
                                this.setDamage(stack, getDamage(stack) + 1);
                            }
                        } else {
                            if (worldIn.destroyBlock(pos.offset(entityLiving.getHorizontalFacing()), true)) {
                                this.setDamage(stack, getDamage(stack) + 1);
                            }
                        }
                        break;
                    case MODE_3x3:
                        EnumFacing.Axis axis = EnumFacing.getDirectionFromEntityLiving(pos, entityLiving).getAxis();
                        for (int i = 0; i < 3; i++) {
                            for (int j = 0; j < 3; j++) {
                                switch (axis) {
                                    case X:
                                        if (worldIn.destroyBlock(pos.add(0, i - 1, j - 1), true)) {
                                            this.setDamage(stack, getDamage(stack) + 1);
                                        }
                                        break;
                                    case Y:
                                        if (worldIn.destroyBlock(pos.add(i - 1, 0, j - 1), true)) {
                                            this.setDamage(stack, getDamage(stack) + 1);
                                        }
                                        break;
                                    case Z:
                                        if (worldIn.destroyBlock(pos.add(j - 1, i - 1, 0), true)) {
                                            this.setDamage(stack, getDamage(stack) + 1);
                                        }
                                        break;
                                }
                            }
                        }
                        break;
                }
            } else {
                this.setDamage(stack, getDamage(stack) + 1);
            }
        }
        return true;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public boolean isFull3D() {
        return true;
    }

    @Override
    public void onCreated(ItemStack stack, World worldIn, EntityPlayer playerIn) {
        stack.setTagInfo(NBT_DAMAGE, new NBTTagInt(0));
        stack.setTagInfo(NBT_MODE, new NBTTagString(MODE_1x1));
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        tooltip.add(String.format("%sRight Click: %sPlace Torch From Inventory", TextFormatting.GREEN.toString(), TextFormatting.WHITE.toString()));
        tooltip.add(String.format("%sDurability: %s%d/%d", TextFormatting.YELLOW, TextFormatting.WHITE, getMaxDamage(stack) - getDamage(stack), 1024));
        if (GuiScreen.isShiftKeyDown()) {
            NBTBase tagInfo = getTagInfo(stack, NBT_MODE);
            if (tagInfo instanceof NBTTagString) {
                tooltip.add(String.format("%sMode: %s%s", TextFormatting.BLUE, TextFormatting.WHITE, ((NBTTagString) tagInfo).getString()));
            }
        } else {
            tooltip.add(String.format("%sPress %sSHIFT %sto see more", TextFormatting.WHITE, TextFormatting.BLUE, TextFormatting.WHITE));
        }
    }

    @Override
    public int getItemEnchantability() {
        return 20;
    }

    @Override
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
        if (this.isInCreativeTab(tab)) {
            ItemStack stack = new ItemStack(this);
            stack.setTagInfo(NBT_DAMAGE, new NBTTagInt(0));
            stack.setTagInfo(NBT_MODE, new NBTTagString(MODE_1x1));
            items.add(stack);
        }
    }

    @Override
    public void onUsingTick(ItemStack stack, EntityLivingBase player, int count) {
        World world = player.world;
        if (!world.isRemote) {
            List<EntityItem> items = world.getEntitiesWithinAABB(EntityItem.class, new AxisAlignedBB(player.getPosition()).offset(.5, .5, .5).grow(5));
            for (EntityItem item : items) {
                Vec3d pull = item.getPositionVector().subtract(player.getPositionVector()).normalize();
                item.motionX = pull.x;
                item.motionY = pull.y;
                item.motionZ = pull.z;
            }
        }
    }

    @Override
    public int getDamage(ItemStack stack) {
        NBTBase damage = getTagInfo(stack, NBT_DAMAGE);
        if (damage != null && damage instanceof NBTTagInt) {
            return ((NBTTagInt) damage).getInt();
        } else {
            int fully_damaged = getMaxDamage(stack);
            stack.setTagInfo(NBT_DAMAGE, new NBTTagInt(fully_damaged));
            return fully_damaged;
        }
    }

    @Override
    public int getMaxDamage(ItemStack stack) {
        return MAX_DURABILITY;
    }

    @Override
    public boolean isDamaged(ItemStack stack) {
        return getDamage(stack) < getMaxDamage(stack);
    }

    @Override
    public void setDamage(ItemStack stack, int damage) {
        stack.setTagInfo(NBT_DAMAGE, new NBTTagInt(damage));
    }

    @Override
    public Set<String> getToolClasses(ItemStack stack) {
        return ImmutableSet.of("shovel", "pickaxe");
    }

    private void cycleMode(ItemStack stack) {
        NBTBase tagInfo = getTagInfo(stack, NBT_MODE);
        if (tagInfo != null && tagInfo instanceof NBTTagString) {
            String mode = ((NBTTagString) tagInfo).getString();
            switch (mode) {

                case MODE_1x1:
                    mode = MODE_1x2;
                    break;
                case MODE_1x2:
                    mode = MODE_3x3;
                    break;
                case MODE_3x3:
                    mode = MODE_1x1;
                    break;
            }
            stack.setTagInfo(NBT_MODE, new NBTTagString(mode));
        }
    }

    private NBTBase getTagInfo(ItemStack stack, String key) {
        NBTTagCompound nbt = stack.getTagCompound();
        if (nbt == null) {
            nbt = new NBTTagCompound();
        }
        return nbt.getTag(key);
    }
}
