package io.gitlab.bluemonster122.sparked.common.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

import java.util.Random;

public class MessageSpawnParticles implements IMessage, IMessageHandler<MessageSpawnParticles, IMessage> {

    private static final Random RANDOM = new Random();
    public int particleID, amount;
    public int dimension;
    public double centerX, centerY, centerZ;
    public double spreadX, spreadY, spreadZ;

    public MessageSpawnParticles() {
    }

    public MessageSpawnParticles(int particleID, int amount, int dimension, double centerX, double centerY, double centerZ, double spreadX, double spreadY, double spreadZ) {
        this.particleID = particleID;
        this.amount = amount;
        this.dimension = dimension;
        this.centerX = centerX;
        this.centerY = centerY;
        this.centerZ = centerZ;
        this.spreadX = spreadX;
        this.spreadY = spreadY;
        this.spreadZ = spreadZ;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.particleID = buf.readInt();
        this.amount = buf.readInt();
        this.dimension = buf.readInt();
        this.centerX = buf.readLong();
        this.centerY = buf.readLong();
        this.centerZ = buf.readLong();
        this.spreadX = buf.readLong();
        this.spreadY = buf.readLong();
        this.spreadZ = buf.readLong();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.particleID);
        buf.writeInt(this.amount);
        buf.writeInt(this.dimension);
        buf.writeDouble(this.centerX);
        buf.writeDouble(this.centerY);
        buf.writeDouble(this.centerZ);
        buf.writeDouble(this.spreadX);
        buf.writeDouble(this.spreadY);
        buf.writeDouble(this.spreadZ);
    }

    @Override
    public IMessage onMessage(MessageSpawnParticles message, MessageContext ctx) {
        Minecraft mc = Minecraft.getMinecraft();
        mc.addScheduledTask(() -> {
            WorldClient world = mc.world;
            if (world.provider.getDimension() == message.dimension) {
                for (int i = 0; i < message.amount; i++) {
                    double x = message.centerX + message.spreadX * (RANDOM.nextFloat() * 2 - 1);
                    double y = message.centerY + message.spreadY * (RANDOM.nextFloat() * 2 - 1);
                    double z = message.centerZ + message.spreadZ * (RANDOM.nextFloat() * 2 - 1);
                    mc.effectRenderer.spawnEffectParticle(message.particleID, x, y, z, x - message.centerX, y - message.centerY, z - message.centerZ);
                }
            }
        });
        return null;
    }
}
