package io.gitlab.bluemonster122.sparked.common.block.mapper;

import io.gitlab.bluemonster122.sparked.common.block.property.EnumWoodType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

public class LogStateMapper extends SimpleStateMapper {

    private final EnumWoodType type;

    public LogStateMapper(String blockStateFileName, EnumWoodType type) {
        super(blockStateFileName);
        this.type = type;
    }

    @Override
    public ModelResourceLocation getModelResourceLocation(IBlockState state) {
        String propertyString = getPropertyString(state.getProperties());
        propertyString += ",type=" + type.getName();
        return new ModelResourceLocation("sparked:" + blockStateFileName, propertyString);
    }
}
