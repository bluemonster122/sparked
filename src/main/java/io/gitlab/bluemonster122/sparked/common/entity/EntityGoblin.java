package io.gitlab.bluemonster122.sparked.common.entity;

import io.gitlab.bluemonster122.sparked.Sparked;
import io.gitlab.bluemonster122.sparked.common.reference.ModInfo;
import io.gitlab.bluemonster122.sparked.common.reference.Names;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementProgress;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class EntityGoblin extends EntityMob {

    public static final ResourceLocation GOBLIN_LOOT = new ResourceLocation(ModInfo.MOD_ID, "entities/goblin");

    public EntityGoblin(World world, Vec3d position) {
        this(world);
        this.setPosition(position.x, position.y, position.z);
    }

    public EntityGoblin(World worldIn) {
        super(worldIn);
    }

    @Override
    public void knockBack(Entity entityIn, float strength, double xRatio, double zRatio) {
        super.knockBack(entityIn, strength, xRatio, zRatio);
    }

    @Override
    protected void initEntityAI() {
        super.initEntityAI();
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(1, new AIGoblinAttackMelee(this, 1, true));
        this.tasks.addTask(2, new EntityAIMoveTowardsRestriction(this, 1));
        this.tasks.addTask(3, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
        this.targetTasks.addTask(1, new AIGoblinAttack(this));
    }

    @Nullable
    @Override
    protected ResourceLocation getLootTable() {
        return GOBLIN_LOOT;
    }

    @Override
    public boolean attackEntityAsMob(Entity entityIn) {
        double x = entityIn.motionX;
        double y = entityIn.motionY;
        double z = entityIn.motionZ;
        int inv = entityIn.hurtResistantTime;
        boolean ret = super.attackEntityAsMob(entityIn);
        entityIn.motionX = x;
        entityIn.motionY = y;
        entityIn.motionZ = z;
        entityIn.hurtResistantTime = inv;
        return ret;
    }

    @Override
    public boolean getCanSpawnHere() {
        return false;
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.5D);
        this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(1.4D);
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10D);
        this.getEntityAttribute(SharedMonsterAttributes.ARMOR).setBaseValue(15D);
        this.getEntityAttribute(SharedMonsterAttributes.ARMOR_TOUGHNESS).setBaseValue(10D);
    }

    @Override
    public boolean isNonBoss() {
        return false;
    }

    private static class AIGoblinAttackMelee extends EntityAIAttackMelee {

        public AIGoblinAttackMelee(EntityCreature creature, double speedIn, boolean useLongMemory) {
            super(creature, speedIn, useLongMemory);
        }

        @Override
        protected void checkAndPerformAttack(EntityLivingBase enemy, double distToEnemySqr) {
            double d0 = this.getAttackReachSqr(enemy);

            if (distToEnemySqr <= d0 && this.attackTick <= 0) {
                this.attackTick = 5;
                this.attacker.swingArm(EnumHand.MAIN_HAND);
                this.attacker.attackEntityAsMob(enemy);
            }
        }
    }

    private static class AIGoblinAttack extends EntityAINearestAttackableTarget<EntityPlayer> {

        public AIGoblinAttack(EntityGoblin goblin) {
            super(goblin, EntityPlayer.class, true);
        }

        @Override
        protected boolean isSuitableTarget(@Nullable EntityLivingBase target, boolean includeInvincibles) {
            if (target instanceof EntityPlayerMP) {
                EntityPlayerMP playerMP = (EntityPlayerMP) target;
                Advancement ignitionAdvancement = playerMP.getServerWorld().getAdvancementManager().getAdvancement(Names.PROGRESSION_SPARK);
                if (ignitionAdvancement == null) {
                    Sparked.logger.error("ignition advancement not found.");
                } else {
                    AdvancementProgress progress = playerMP.getAdvancements().getProgress(ignitionAdvancement);
                    return !progress.isDone();
                }
            }
            return false;
        }
    }
}
