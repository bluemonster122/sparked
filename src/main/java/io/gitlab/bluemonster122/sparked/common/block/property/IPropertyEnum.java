package io.gitlab.bluemonster122.sparked.common.block.property;

import net.minecraft.util.IStringSerializable;

public interface IPropertyEnum extends IStringSerializable {

    int getMetadata();
}
