package io.gitlab.bluemonster122.sparked.common.block;

import io.gitlab.bluemonster122.sparked.common.SItems;
import io.gitlab.bluemonster122.sparked.common.block.mapper.LogStateMapper;
import io.gitlab.bluemonster122.sparked.common.block.property.EnumWoodType;
import net.minecraft.block.BlockLog.EnumAxis;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;

import static net.minecraft.block.BlockLog.LOG_AXIS;

public class BlockMagicalLog extends SBlock implements IModelRegisterer {

    private EnumWoodType type;

    public BlockMagicalLog(EnumWoodType type) {
        super(Material.WOOD);
        this.type = type;
        this.setDefaultState(getDefaultState().withProperty(LOG_AXIS, EnumAxis.Y));
    }

    @Deprecated
    @Override
    public IBlockState getStateFromMeta(int meta) {
        return getDefaultState().withProperty(LOG_AXIS, EnumAxis.values()[meta & 3]);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(LOG_AXIS).ordinal() & 3;
    }

    @Override
    public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items) {
        items.add(new ItemStack(this, 1, EnumAxis.Y.ordinal()));
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer.Builder(this).add(LOG_AXIS).build();
    }

    @Override
    public boolean canSustainLeaves(IBlockState state, IBlockAccess world, BlockPos pos) {
        return state.getBlock() == this || world == null || pos == null || world.getBlockState(pos).getBlock() == this;
    }

    @Override
    public boolean isWood(IBlockAccess world, BlockPos pos) {
        return world == null || pos == null || world.getBlockState(pos).getBlock() == this;
    }

    @Override
    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer, EnumHand hand) {
        EnumHand handToCheck = hand == EnumHand.OFF_HAND ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND;
        if (placer.getHeldItem(handToCheck).getItem() == SItems.STICK_MAGICAL) {
            return getDefaultState().withProperty(LOG_AXIS, EnumAxis.NONE);
        }
        return getDefaultState().withProperty(LOG_AXIS, EnumAxis.fromFacingAxis(facing.getAxis()));
    }

    @Override
    public void initModel() {
        LogStateMapper mapper = new LogStateMapper("log_magical", getType());
        ModelLoader.setCustomStateMapper(this, mapper);
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0, mapper.getModelResourceLocation(getStateFromMeta(EnumAxis.Y.ordinal())));
    }

    public EnumWoodType getType() {
        return type;
    }
}
