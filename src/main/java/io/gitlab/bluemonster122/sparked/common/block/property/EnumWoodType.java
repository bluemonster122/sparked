package io.gitlab.bluemonster122.sparked.common.block.property;

import io.gitlab.bluemonster122.sparked.common.reference.ModInfo;
import net.minecraft.util.ResourceLocation;

import java.util.Locale;

public enum EnumWoodType implements IPropertyEnum {
    REGULAR, COLORLESS, WHITE, BLUE, BLACK, GREEN, RED;

    public static final EnumWoodType[] VALUES = values();

    private ResourceLocation location;

    public static EnumWoodType fromMeta(int meta) {
        if (meta < 0 || meta >= VALUES.length) {
            meta = 0;
        }
        return VALUES[meta];
    }

    public ResourceLocation getResourceLocation() {
        if (location == null) {
            location = new ResourceLocation(ModInfo.MOD_ID, "log_magical_" + getName());
        }
        return location;
    }

    @Override
    public String getName() {
        return toString();
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase(Locale.ENGLISH);
    }

    @Override
    public int getMetadata() {
        return ordinal();
    }
}
