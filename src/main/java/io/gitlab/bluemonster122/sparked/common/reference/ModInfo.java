package io.gitlab.bluemonster122.sparked.common.reference;

public final class ModInfo {

    public static final String MOD_ID = "sparked";
    public static final String MOD_NAME = "Sparked";
    public static final String VERSION = "1.0.0";
    public static final String RESOURCE_PREFIX = MOD_ID + ":";
}
