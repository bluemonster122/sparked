package io.gitlab.bluemonster122.sparked.common.block;

import io.gitlab.bluemonster122.sparked.common.STab;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class SBlock extends Block implements IModelRegisterer {

    public SBlock(Material materialIn) {
        super(materialIn);
        setCreativeTab(STab.INSTANCE);
    }
}
