package io.gitlab.bluemonster122.sparked.common.block.mapper;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;

public class SimpleStateMapper extends StateMapperBase {

    protected String blockStateFileName;

    public SimpleStateMapper(String blockStateFileName) {
        this.blockStateFileName = blockStateFileName;
    }

    @Override
    public ModelResourceLocation getModelResourceLocation(IBlockState state) {
        return new ModelResourceLocation("sparked:" + blockStateFileName, getPropertyString(state.getProperties()));
    }
}
