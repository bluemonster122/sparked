package io.gitlab.bluemonster122.sparked.common.util;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Registry {
    private final List<RegistryObject> OBJECTS = new ArrayList<>();
    private String name;

    public Registry(String name, RegistryObject... objects) {
        this.name = name;
        MinecraftForge.EVENT_BUS.register(this);
        Collections.addAll(OBJECTS, objects);
    }

    @SubscribeEvent
    public void onBlockRegister(RegistryEvent.Register<Block> event) {
        OBJECTS.forEach(RegistryObject::registerBlocks);
    }

    @SubscribeEvent
    public void onItemRegister(RegistryEvent.Register<Item> event) {
        OBJECTS.forEach(RegistryObject::registerItems);
    }

    @SubscribeEvent
    public void onModelReigster(ModelRegistryEvent event) {
        OBJECTS.forEach(RegistryObject::registerModels);
    }

    public String getName() {
        return name;
    }

    public void registerEntities() {
        OBJECTS.forEach(RegistryObject::registerEntities);
    }

    public void registerAdvancements() {
        OBJECTS.forEach(RegistryObject::registerAdvancements);
    }

    public void registerOres() {
        OBJECTS.forEach(RegistryObject::registerOreDict);
    }
}
