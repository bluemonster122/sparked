package io.gitlab.bluemonster122.sparked.common.objects;

import io.gitlab.bluemonster122.sparked.common.SItems;
import io.gitlab.bluemonster122.sparked.common.item.ItemMiningTool;
import io.gitlab.bluemonster122.sparked.common.reference.Names;
import io.gitlab.bluemonster122.sparked.common.util.RegistryObject;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Tools extends RegistryObject {
    public Tools() {
        super("tools");
    }

    @Override
    public void registerBlocks() {
        /* NOOP */
    }

    @Override
    public void registerItems() {
        registerItem(new ItemMiningTool(), Names.MINING_TOOL);
    }

    @Override
    public void registerModels() {
        SItems.MINING_TOOL.initModel();
    }

    @Override
    public void registerOreDict() {
        /* NOOP */
    }

    @Override
    public void registerEntities() {
        /* NOOP */
    }

    @Override
    public void registerAdvancements() {
        /* NOOP */
    }
}
