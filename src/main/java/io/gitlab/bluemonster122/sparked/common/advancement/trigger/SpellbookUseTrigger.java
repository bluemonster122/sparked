package io.gitlab.bluemonster122.sparked.common.advancement.trigger;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import io.gitlab.bluemonster122.sparked.common.reference.ModInfo;
import net.minecraft.advancements.ICriterionTrigger;
import net.minecraft.advancements.PlayerAdvancements;
import net.minecraft.advancements.critereon.AbstractCriterionInstance;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;

public class SpellbookUseTrigger implements ICriterionTrigger<SpellbookUseTrigger.Instance> {

    private static final ResourceLocation ID = new ResourceLocation(ModInfo.MOD_ID, "used_spellbook");
    public static SpellbookUseTrigger SPELLBOOK_USE_TRIGGER;
    private final HashMultimap<PlayerAdvancements, Listener<Instance>> instances = HashMultimap.create();

    @Override
    public ResourceLocation getId() {
        return ID;
    }

    @Override
    public void addListener(PlayerAdvancements playerAdvancementsIn, Listener<Instance> listener) {
        instances.put(playerAdvancementsIn, listener);
    }

    @Override
    public void removeListener(PlayerAdvancements playerAdvancementsIn, Listener<Instance> listener) {
        instances.remove(playerAdvancementsIn, listener);
    }

    @Override
    public void removeAllListeners(PlayerAdvancements playerAdvancementsIn) {
        instances.removeAll(playerAdvancementsIn);
    }

    @Override
    public Instance deserializeInstance(JsonObject json, JsonDeserializationContext context) {
        Item item = JsonUtils.getItem(json, "item");
        return new Instance(item);
    }

    public void trigger(EntityPlayerMP playerMP, Item item) {
        for (Listener<Instance> listener : Lists.newArrayList(instances.get(playerMP.getAdvancements()))) {
            if (listener.getCriterionInstance().item == item) {
                listener.grantCriterion(playerMP.getAdvancements());
            }
        }
    }

    public static class Instance extends AbstractCriterionInstance {

        private final Item item;

        public Instance(Item item) {
            super(ID);
            this.item = item;
        }
    }
}
