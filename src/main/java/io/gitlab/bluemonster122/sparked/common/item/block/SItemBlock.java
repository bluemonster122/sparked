package io.gitlab.bluemonster122.sparked.common.item.block;

import io.gitlab.bluemonster122.sparked.common.STab;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;

public class SItemBlock extends ItemBlock {
    public SItemBlock(Block block) {
        super(block);
    }

    @Override
    public CreativeTabs getCreativeTab() {
        return STab.INSTANCE;
    }
}
