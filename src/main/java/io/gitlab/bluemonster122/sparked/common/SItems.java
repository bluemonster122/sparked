package io.gitlab.bluemonster122.sparked.common;

import io.gitlab.bluemonster122.sparked.common.item.ItemMiningTool;
import io.gitlab.bluemonster122.sparked.common.item.ItemSpellbook;
import io.gitlab.bluemonster122.sparked.common.item.SItem;
import io.gitlab.bluemonster122.sparked.common.reference.ModInfo;
import net.minecraftforge.fml.common.registry.GameRegistry.ObjectHolder;

@ObjectHolder(ModInfo.MOD_ID)
public class SItems {
    /*
     * WOOD
     */
    public static final SItem STICK_MAGICAL = null;
    /*
     * TOOLS
     */
    public static final ItemMiningTool MINING_TOOL = null;
    /*
     * PROGRESSION
     */
    public static final SItem PWSPARK = null;
    public static final ItemSpellbook SPELLBOOK = null;
}
