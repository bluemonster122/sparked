package io.gitlab.bluemonster122.sparked.common.reference;

import net.minecraft.util.ResourceLocation;

public class Names {
    /*
     * WOOD
     */
    public static final ResourceLocation LOG_MAGICAL_REGULAR = new ResourceLocation(ModInfo.MOD_ID, "log_magical_regular");
    public static final ResourceLocation LOG_MAGICAL_COLORLESS = new ResourceLocation(ModInfo.MOD_ID, "log_magical_colorless");
    public static final ResourceLocation LOG_MAGICAL_WHITE = new ResourceLocation(ModInfo.MOD_ID, "log_magical_white");
    public static final ResourceLocation LOG_MAGICAL_BLUE = new ResourceLocation(ModInfo.MOD_ID, "log_magical_blue");
    public static final ResourceLocation LOG_MAGICAL_BLACK = new ResourceLocation(ModInfo.MOD_ID, "log_magical_black");
    public static final ResourceLocation LOG_MAGICAL_RED = new ResourceLocation(ModInfo.MOD_ID, "log_magical_red");
    public static final ResourceLocation LOG_MAGICAL_GREEN = new ResourceLocation(ModInfo.MOD_ID, "log_magical_green");
    public static final ResourceLocation PLANKS_MAGICAL = new ResourceLocation(ModInfo.MOD_ID, "planks_magical");
    public static final ResourceLocation STICK_MAGICAL = new ResourceLocation(ModInfo.MOD_ID, "stick_magical");
    /*
     * TOOLS
     */
    public static final ResourceLocation MINING_TOOL = new ResourceLocation(ModInfo.MOD_ID, "mining_tool");
    /*
     * PROGRESSION
     */
    public static final ResourceLocation PWSPARK = new ResourceLocation(ModInfo.MOD_ID, "pwspark");
    public static final ResourceLocation SPELLBOOK = new ResourceLocation(ModInfo.MOD_ID, "spellbook");
    public static final ResourceLocation GOBLIN = new ResourceLocation(ModInfo.MOD_ID, "goblin");
    public static final ResourceLocation PROGRESSION_ROOT = new ResourceLocation(ModInfo.MOD_ID, "progression/root");
    public static final ResourceLocation PROGRESSION_SPARK = new ResourceLocation(ModInfo.MOD_ID, "progression/spark");
    public static final ResourceLocation PROGRESSION_SPELLBOOK = new ResourceLocation(ModInfo.MOD_ID, "progression/open_spellbook");
}
