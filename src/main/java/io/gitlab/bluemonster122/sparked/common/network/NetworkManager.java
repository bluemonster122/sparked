package io.gitlab.bluemonster122.sparked.common.network;

import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public final class NetworkManager {

    public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel("assets/sparked");

    public static void init() {
        INSTANCE.registerMessage(MessageSpawnParticles.class, MessageSpawnParticles.class, 0, Side.CLIENT);
    }
}
