package io.gitlab.bluemonster122.sparked.client.gui;

import io.gitlab.bluemonster122.sparked.common.container.ContainerSpellbook;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

import javax.annotation.Nullable;

public class GuiHandler implements IGuiHandler {

    @Nullable
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        Object container;
        switch (ID) {
            case 0:
                container = new ContainerSpellbook(player);
                break;
            default:
                container = null;
                break;
        }
        return container;
    }

    @Nullable
    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        Object gui;
        switch (ID) {
            case 0:
                gui = new GuiSpellbook(player, new ContainerSpellbook(player));
                break;
            default:
                gui = null;
                break;
        }
        return gui;
    }
}
