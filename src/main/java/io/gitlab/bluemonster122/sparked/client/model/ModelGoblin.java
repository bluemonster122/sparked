package io.gitlab.bluemonster122.sparked.client.model;

import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

/**
 * ModelGoblin - BlueMosnter122
 * Created using Tabula 7.0.0
 */
public class ModelGoblin extends ModelBiped {
    public ModelRenderer bipedRightEarBase;
    public ModelRenderer bipedLeftEarBase;
    public ModelRenderer bipedNose;
    public ModelRenderer bipedRightEarEnd;
    public ModelRenderer bipedLeftEarEnd;

    public ModelGoblin() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.bipedRightLeg = new ModelRenderer(this, 0, 16);
        this.bipedRightLeg.setRotationPoint(-1.899999976158142F, 12.0F, 0.10000000149011612F);
        this.bipedRightLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, 0.0F);
        this.bipedRightEarBase = new ModelRenderer(this, 32, 0);
        this.bipedRightEarBase.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.bipedRightEarBase.addBox(-2.0F, -7.2F, -1.5F, 3, 3, 3, 0.0F);
        this.setRotateAngle(bipedRightEarBase, 0.0F, 0.0F, -0.6108652381980153F);
        this.bipedNose = new ModelRenderer(this, 36, 15);
        this.bipedNose.setRotationPoint(-0.5F, 0.0F, 0.0F);
        this.bipedNose.addBox(0.0F, -3.5F, -5.0F, 1, 2, 1, 0.0F);
        this.setRotateAngle(bipedNose, -0.21467549799530256F, 0.0F, 0.0F);
        this.bipedLeftArm = new ModelRenderer(this, 40, 16);
        this.bipedLeftArm.mirror = true;
        this.bipedLeftArm.setRotationPoint(5.0F, 2.5F, 0.0F);
        this.bipedLeftArm.addBox(-1.0F, -2.0F, -2.0F, 3, 12, 4, 0.0F);
        this.setRotateAngle(bipedLeftArm, 0.0F, 0.0F, -0.10000736613927509F);
        this.bipedBody = new ModelRenderer(this, 16, 16);
        this.bipedBody.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.bipedBody.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, 0.0F);
        this.bipedLeftEarBase = new ModelRenderer(this, 32, 0);
        this.bipedLeftEarBase.mirror = true;
        this.bipedLeftEarBase.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.bipedLeftEarBase.addBox(-1.0F, -7.2F, -1.5F, 3, 3, 3, 0.0F);
        this.setRotateAngle(bipedLeftEarBase, 0.0F, 0.0F, 0.6108652381980153F);
        this.bipedRightEarEnd = new ModelRenderer(this, 32, 6);
        this.bipedRightEarEnd.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.bipedRightEarEnd.addBox(1.0F, -11.9F, -1.0F, 2, 7, 2, 0.0F);
        this.setRotateAngle(bipedRightEarEnd, 0.0F, 0.0F, -0.5235987755982988F);
        this.bipedHead = new ModelRenderer(this, 0, 0);
        this.bipedHead.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.bipedHead.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, 0.0F);
        this.bipedRightArm = new ModelRenderer(this, 40, 16);
        this.bipedRightArm.setRotationPoint(-5.0F, 2.5F, 0.0F);
        this.bipedRightArm.addBox(-2.0F, -2.0F, -2.0F, 3, 12, 4, 0.0F);
        this.setRotateAngle(bipedRightArm, 0.0F, 0.0F, 0.10000000149011613F);
        this.bipedLeftLeg = new ModelRenderer(this, 0, 16);
        this.bipedLeftLeg.mirror = true;
        this.bipedLeftLeg.setRotationPoint(1.9F, 12.0F, 0.1F);
        this.bipedLeftLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, 0.0F);
        this.bipedLeftEarEnd = new ModelRenderer(this, 32, 6);
        this.bipedLeftEarEnd.mirror = true;
        this.bipedLeftEarEnd.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.bipedLeftEarEnd.addBox(-3.0F, -11.8F, -1.0F, 2, 7, 2, 0.0F);
        this.setRotateAngle(bipedLeftEarEnd, 0.0F, 0.0F, 0.5235987755982988F);
        this.bipedHead.addChild(this.bipedRightEarBase);
        this.bipedHead.addChild(this.bipedNose);
        this.bipedHead.addChild(this.bipedLeftEarBase);
        this.bipedRightEarBase.addChild(this.bipedRightEarEnd);
        this.bipedLeftEarBase.addChild(this.bipedLeftEarEnd);
        isChild = true;
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        this.bipedRightLeg.render(f5);
        this.bipedLeftArm.render(f5);
        this.bipedBody.render(f5);
        this.bipedHead.render(f5);
        this.bipedRightArm.render(f5);
        this.bipedLeftLeg.render(f5);
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
