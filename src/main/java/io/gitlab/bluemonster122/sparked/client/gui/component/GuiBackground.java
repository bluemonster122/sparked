package io.gitlab.bluemonster122.sparked.client.gui.component;

import net.minecraft.client.Minecraft;

public class GuiBackground extends GuiComponent {

    public GuiBackground(int tX, int tY, int pX, int pY, int width, int height, Runnable preRunTask) {
        super(tX, tY, pX, pY, width, height, preRunTask);
    }


    @Override
    public void initElement(Minecraft mc) {
        // NOOP
    }

    @Override
    public void drawBackground(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        drawTexturedModalRect(getXPos(), getYPos(), getXTex(), getYTex(), getWidth(), getHeight());
    }

    @Override
    public void drawForeground(Minecraft mc, int mouseX, int mouseY) {
        // NOOP
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        // NOOP
    }

    @Override
    public String getName() {
        return "background";
    }
}
