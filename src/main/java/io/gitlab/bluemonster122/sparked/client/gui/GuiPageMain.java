package io.gitlab.bluemonster122.sparked.client.gui;

import io.gitlab.bluemonster122.sparked.client.gui.component.GuiComponent;
import net.minecraft.client.Minecraft;

public class GuiPageMain extends GuiComponent {

    public Category[] categories;
    private GuiSpellbook parent;

    public GuiPageMain(GuiSpellbook parent, int left, int top, Runnable preDrawTask) {
        super(0, 0, left + 14, top + 13, 115, 153, preDrawTask);
        this.parent = parent;
    }

    @Override
    public void initElement(Minecraft mc) {
        categories = new Category[]{new Category("crafting", 6, 4, 1, 1, 28, 28)};
    }

    @Override
    public void drawBackground(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        for (Category c : categories) {
            drawTexturedModalRect(getXPos() + c.x, getYPos() + c.y, c.tx, c.ty, c.width, c.height);
        }
    }

    @Override
    public void drawForeground(Minecraft mc, int mouseX, int mouseY) {
        int xMin;
        int yMin;
        int xMax;
        int yMax;
        for (Category c : categories) {
            xMin = c.x + getXPos();
            yMin = c.y + getYPos();
            xMax = c.x + c.width + getXPos();
            yMax = c.y + c.height + getYPos();
            if (xMin <= mouseX && mouseX < xMax && yMin <= mouseY && mouseY < yMax) {
                drawTexturedModalRect(getXPos() + c.x, getYPos() + c.y, c.tx, c.ty + 128, c.width, c.height);
            }
        }
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        int xMin;
        int yMin;
        int xMax;
        int yMax;
        for (Category c : categories) {
            xMin = c.x + getXPos();
            yMin = c.y + getYPos();
            xMax = c.x + c.width + getXPos();
            yMax = c.y + c.height + getYPos();
            if (xMin <= mouseX && mouseX <= xMax && yMin <= mouseY && mouseY <= yMax) {
                parent.openPage(c.getId());
            }
        }
    }

    @Override
    public String getName() {
        return "Spellbook - Main";
    }

    public class Category {
        public int x;
        public int y;
        public int tx;
        public int ty;
        public int width;
        public int height;
        private String id;

        public Category(String id, int x, int y, int tx, int ty) {
            this(id, x, y, tx, ty, 32, 32);
        }

        public Category(String id, int x, int y, int tx, int ty, int width, int height) {
            this.id = id;
            this.x = x;
            this.y = y;
            this.tx = tx;
            this.ty = ty;
            this.width = width;
            this.height = height;
        }

        public String getId() {
            return id;
        }
    }
}
