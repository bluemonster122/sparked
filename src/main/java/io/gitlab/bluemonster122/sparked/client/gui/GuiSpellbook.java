package io.gitlab.bluemonster122.sparked.client.gui;

import io.gitlab.bluemonster122.sparked.client.gui.component.GuiBackground;
import io.gitlab.bluemonster122.sparked.client.gui.component.GuiComponent;
import io.gitlab.bluemonster122.sparked.common.container.ContainerSpellbook;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.ResourceLocation;

public class GuiSpellbook extends GuiBase {

    private static final ResourceLocation BOOK = new ResourceLocation("sparked", "textures/gui/spellbook.png");
    private final Runnable bindIcons = () -> mc.getTextureManager().bindTexture(S_ICONS);
    private PageButton lastPage;
    private PageButton nextPage;
    private GuiComponent currentPage;

    public GuiSpellbook(EntityPlayer player, ContainerSpellbook containerSpellbook) {
        super(containerSpellbook);
        this.xSize = 146;
        this.ySize = 180;
    }

    @Override
    public void initGui() {
        int i = (width - xSize) / 2;
        int j = (height - ySize) / 2;
        nextPage = addButton(new PageButton(0, i + 123, height - 24, true));
        lastPage = addButton(new PageButton(0, i, height - 24, false));
        addComponent(new GuiBackground(20, 1, i, j, 146, 180, () -> mc.getTextureManager().bindTexture(BOOK)));
        currentPage = addComponent(new GuiPageMain(this, i, j, bindIcons));
        super.initGui();
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        super.drawGuiContainerForegroundLayer(mouseX, mouseY);
        String title = currentPage.getName();
        int stringWidth = mc.fontRenderer.getStringWidth(title);
        int y = (int) ((height - ySize) / 2 - mc.fontRenderer.FONT_HEIGHT * 1.5);
        int x = (width - stringWidth) / 2;
        drawString(mc.fontRenderer, title, x, y, 0xFF00AAEE);
    }

    @Override
    protected void actionPerformed(GuiButton button) {

    }

    public void openPage(String pageId) {
        removeComponent(currentPage);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        switch (pageId) {
            case "crafting":
                currentPage = addComponent(new GuiPageCrafting(this, i, j, bindIcons));
                break;
            case "main":
            default:
                currentPage = addComponent(new GuiPageMain(this, i, j, bindIcons));
                break;
        }
        currentPage.initElement(mc);
        mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1.0F));
    }

    public class PageButton extends GuiButton {

        private boolean next;

        public PageButton(int buttonId, int x, int y, boolean next) {
            super(buttonId, x, y, 18, 10, "");
            this.next = next;
        }

        @Override
        public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
            if (this.visible) {
                boolean flag = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
                GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
                int i = 0;
                int j = 192;

                if (flag) {
                    i += 23;
                }

                if (!this.next) {
                    j += 13;
                }

                this.drawTexturedModalRect(this.x, this.y, i, j, 23, 13);
            }
        }
    }
}
